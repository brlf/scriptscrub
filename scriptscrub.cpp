/* Script Scrub - Produce cleaned up `script` output
 * Copyright (C) 2024  brie (gitlab.com/brlf)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <chrono>
#include <thread>

// #define DEBUG_ESCAPES

#ifdef DEBUG_ESCAPES
#define DEBUG_PRINT(x) std::cerr << x << "\n";
#else
#define DEBUG_PRINT(x)
#endif

struct Cell {
	struct Mode {
		// When indexed as 256 color, fg >= 256 and (fg - 256) is the real index
		// Same with bg
		size_t fg = 39, bg = 49;
		bool bold = false, dim = false;
		size_t font;

		enum Invert {
			InvertOn = 7,
			InvertOff = 27,
		} invert = InvertOff;

		enum Blink {
			BlinkOn = 5,
			BlinkOff = 25,
		} blink = BlinkOff; // blink / background brightness

		Mode() {reset();}

		void reset() {
			fg = 39;
			bg = 49;
			font = 10;
			bold = dim = false;
			invert = InvertOff;
			blink = BlinkOff;
		}

		std::string to_string(Mode current) {
			size_t escapes = 0;
			std::stringstream buf;
			buf << "\033[";

#define ADD_ESCAPE(num) \
			{ \
				if (escapes++) \
					buf << ";"; \
				buf << num; \
			}

#define CHANGED(var) (var != current.var)
#define UPDATE_ESCAPE(var) \
			{ \
				if (CHANGED(var)) \
					ADD_ESCAPE(var); \
			}

			if (CHANGED(fg) && fg < 256)
				ADD_ESCAPE(fg);
			if (CHANGED(bg) && bg < 256)
				ADD_ESCAPE(bg);
			UPDATE_ESCAPE(invert);
			UPDATE_ESCAPE(blink);
			UPDATE_ESCAPE(font);

			if ((current.bold & !bold) || (current.dim && !dim)) {
				ADD_ESCAPE(22);
				if (bold)
					ADD_ESCAPE(1);
				if (dim)
					ADD_ESCAPE(2);
			} else {
				if (bold && !current.bold)
					ADD_ESCAPE(1);
				if (dim && !current.dim)
					ADD_ESCAPE(2);
			}

#undef ADD_ESCAPE
#undef UPDATE_ESCAPE

			buf << "m";

			if (!escapes)
				buf.str({});

			if (CHANGED(fg) && fg >= 256)
				buf << "\033[38;5;" << fg - 256 << "m";

			if (CHANGED(bg) && bg >= 256)
				buf << "\033[48;5;" << bg - 256 << "m";

			return buf.str();
		}

		void apply_one(std::string seq) {
			int n;
			try {
				n = std::stoi(seq);
			} catch (std::invalid_argument e) {
				// x:y sequence?
#ifdef DEBUG_MODES
				std::cerr << "Unknown attribute " << seq << "\n";
#endif
				return;
			} catch (std::out_of_range e) {
#ifdef DEBUG_MODES
				std::cerr << "Attribute out of range " << seq << "\n";
#endif
				return;
			}
			switch (n) {
			case 0:
				reset();
				break;
			case 1:
				bold = true;
				break;
			case 2:
				dim = true;
				break;
			case 22:
				bold = dim = false;
				break;
			case InvertOn:
			case InvertOff:
				invert = (Invert)n;
				break;
			case BlinkOn:
			case BlinkOff:
				blink = (Blink)n;
				break;
			default:
				if (
						(n >= 30 && n <= 39) ||
						(n >= 90 && n <= 97)) {
					fg = (size_t)n;
				} else if (
						(n >= 40 && n <= 49) ||
						(n >= 100 && n <= 107)) {
					bg = (size_t)n;
				} else if (n >= 10 && n <= 19) {
					font = n;
				}
#ifdef DEBUG_ESCAPES
				else {
					std::cerr << "Unknown mode number " << n << "\n";
				}
#endif
			}
		}
		void apply(std::string escape) {
			if (!escape.size()) {
				reset();
				return;
			}

			std::vector<std::string> escapes = {""};
			for (char c : escape) {
				if (c == ';' || c == ':') {
					escapes.push_back("");
				} else {
					escapes.back() += {c};
				}
			}

			for (size_t i = 0; i < escapes.size();) {
				auto &e0 = escapes[i];
				if (i + 2 < escapes.size()) {
					auto &e1 = escapes[i+1];
					auto &e2 = escapes[i+2];
					int color = -1;
					if (sscanf(e2.data(), "%d",&color) && color >= 0) {
						if (e0 == "38" && e1 == "5") {
							fg = color + 256;
							i += 3;
							continue;
						}
						if (e0 == "48" && e1 == "5") {
							bg = color + 256;
							i += 3;
							continue;
						}
					}
				}

				apply_one(e0);
				i++;
			}
		}
	} mode;
	std::string character;
	Cell(std::string ch = " ", Mode m = Mode()) : mode(m), character(ch) {}
};

class Screen {
	std::vector<std::vector<Cell>> buffer;
	size_t height;
	size_t head;

public:
	Screen(size_t w, size_t h) : height(h), head(0) {
		(void)w;
		buffer.resize(h);
	};

	std::vector<Cell> &operator[](size_t r) {
		return buffer[(head + r) % height];
	}

	void scroll_down() {
		(*this)[0].clear();
		head++;
		head %= height;
	}
};


class Buffer {
	enum InputState {
		Normal, Escape, Utf8, ANSI, OSC
	} state = Normal;

public:
	struct Tpos {
		size_t col, row;
	};
private:
	Tpos pos;

	bool wraparound = true;
	bool pending_wrap = false;
	bool alt_mode = false; // use alternative buffer

	struct DecscSave {
		Tpos pos;
		Cell::Mode mode;
	};
	std::vector<DecscSave> decsc_stack;

	size_t width, height;
	Cell::Mode mode = Cell::Mode();
	std::string escape, utf8;
	std::vector<std::vector<Cell>> scrollback;
	Screen buffer, alt_buffer;


public:
	Buffer(size_t w, size_t h) : pos({0, 0}), width(w), height(h), buffer({w, h}), alt_buffer({w, h}) {
	}

	std::vector<Cell> &get_row(size_t r) {
		return alt_mode ? alt_buffer[r] : buffer[r];
	}


	void set_cell(Cell c) {
		get_cell() = c;
	}

	Cell &get_cell() {
		auto &r = get_row(pos.row);
		while (pos.col >= r.size())
			r.push_back(Cell());
		return r[pos.col];
	}

	Tpos get_pos() { return pos; }

	void update_scroll() {
		auto &buf = alt_mode ? alt_buffer : buffer;
		while (pos.row >= height) {
			if (!alt_mode)
				scrollback.push_back(buf[0]);
			buf.scroll_down();
			pos.row--;
		}
	}
	void ret() {
		pos.col = 0;
		pos.row++;
		pending_wrap = false;
		update_scroll();
	}

	void write_character(std::string c) {
		if (pending_wrap && wraparound) {
			ret();
			pending_wrap = false;
		}
		set_cell(Cell(c, mode));
		pos.col++;
		if (pos.col >= width) {
			pos.col = width - 1;
			pending_wrap = true;
		}
	}


	void input(char c) {
		switch (state) {
		case Utf8:
			utf8 += {c};
			if ((utf8[0] << utf8.size()) & 128)
				break;

			write_character(utf8);
			state = Normal;
			break;
		case Normal:
			switch (c) {
			case 27: // ESC
				state = Escape;
				escape = "";
				break;
			case '\n':
				ret();
				break;
			case '\r':
				pending_wrap = false;
				pos.col = 0;
				break;
			case '\t':
				{
					size_t ncol = pos.col + 8;
					ncol -= ncol % 8;
					if (ncol >= width) {
						if (wraparound) {
							ret();
						} else {
							pos.col = width - 1;
							pending_wrap = true;
						}
					} else {
						pos.col = ncol;
					}
				}
				break;
			case '\v':
				pos.row++;
				update_scroll();
			case 0x08: // backspace
				if (pos.col)
					pos.col--;
				pending_wrap = false;
			default:
				if (c & 128) { // UTF-8?
					utf8 = {c};
					state = Utf8;
					break;
				}

				write_character({c});
			}
			break;
		case Escape:
			switch (c) {
			case ']':
				escape = "";
				state = OSC;
				break;
			case '[':
				if (escape.size())
					goto unrecognised_escape;
				state = ANSI;
				break;
			case '7':
				state = Normal;
				decsc_stack.push_back({});
				{
					auto &decsc = decsc_stack.back();
					decsc.mode = mode;
					decsc.pos = pos;
				}
				break;
			case '8':
				state = Normal;
				if (!decsc_stack.size())
					break;
				{
					auto &decsc = decsc_stack.back();
					mode = decsc.mode;
					pos = decsc.pos;
				}
				decsc_stack.pop_back();
				pending_wrap = false;
				state = Normal;
				break;
			default:
unrecognised_escape:
#ifdef DEBUG_ESCAPES
				std::cerr << "Unknown escape: " << (char)c << "\n";
#endif
				state = Normal;
				break;
			}
			break;
		case ANSI:
			switch (c) {
			case 'H':
			case 'f':
				state = Normal;
				{
					int nrow = 0, ncol = 0;
					sscanf(escape.data(), "%d;%d", &nrow, &ncol);

#ifdef DEBUG_ESCAPES
					std::cerr << "Move to " << nrow <<","<< ncol << "\n";
#endif

					pos.row = nrow > 0 ? nrow - 1 : 0;
					if (pos.row >= height)
						pos.row = height - 1;
					pos.col = ncol > 0 ? ncol - 1 : 0;
					if (pos.col >= width)
						pos.col = width - 1;
					pending_wrap = false;
				}
				break;
			case 'h':
			case 'l':
				state = Normal;
				{
					bool question = false;
					if (escape.size() && escape[0] == '?')
						question = true;
					std::string seq;
					if (escape.size())
						escape += ";";
					for (char ch : escape) {
						if (ch == ';') {
							if (seq == "?7") {
								wraparound = c == 'h';
							} else if (seq == "?47") {
								alt_mode = c == 'h';
							} else if (seq == "?1049") {
								alt_mode = c == 'h';
								if (!alt_mode)
									alt_buffer = Screen(width, height);
							}

#ifdef DEBUG_ESCAPES
							else {
								std::cerr << seq << c << "\n";
							}
#endif

							seq = question ? "?" : "";
						} else {
							seq += {ch};
						}
					}
				}
				break;
			case 'm':
				state = Normal;
				mode.apply(escape);
				break;
			case 'J':
				state = Normal;
				if (escape == "" || escape == "0") {
					get_row(pos.row).resize(pos.col);
					for (size_t i = pos.row; i < height; i++)
						get_row(i).clear();
				} else if (escape == "1") {
					auto &crow = get_row(pos.row);
					if (pos.col > crow.size()) {
						crow.clear();
					} else {
						for (size_t co = pos.col; co--;)
							crow[co] = Cell();
					}
					for (auto r = pos.row; r--;)
						get_row(r).clear();
				} // todo other J escapes
				break;
			case 'A':
			case 'B':
			case 'C':
			case 'D':
				state = Normal;
				pending_wrap = false;
				{
					size_t n;
					try {
						int sn = std::stoi(escape);
						if (sn < 0) {
							DEBUG_PRINT("Negative relative move: " << sn);
							break;
						}
						n = sn;
					} catch(...) {
						DEBUG_PRINT("Bad relative move: " << escape);
						break;
					}

					switch (c) {
					case 'A':
						pos.row = pos.row < n ? 0 : pos.row - n;
						break;
					case 'B':
						pos.row += n;
						break;
					case 'C':
						pos.col += n;
						if (pos.col >= width)
							pos.col = width - 1;
						break;
					case 'D':
						pos.col = pos.col < n ? 0 : pos.col - n;
						break;
					}
				}
				break;
			case 'K':
				{
					int cmd = 0;
					sscanf(escape.data(), "%d", &cmd);
					switch (cmd) {
					case 0:
						{
							auto &r = get_row(pos.row);
							r.resize(width);
							for (size_t i = pos.col; i < width; i++)
								r[i] = Cell(" ", mode);
						}
						break;
					case 1:
						{
							auto &r = get_row(pos.row);
							for (size_t i = pos.col+1; i--;)
								r[i] = Cell(" ", mode);
						}
						break;
					case 2:
						{
							auto &r = get_row(pos.row);
							r.resize(width);
							for (Cell &cell : r)
								cell = Cell(" ", mode);
						}
						break;
					case 3:
						if (!pending_wrap) {
							auto &r = get_row(pos.row);
							r.resize(width);
							for (size_t i = pos.col; i < width; i++)
								r[i] = Cell(" ", mode);
						}
						break;
					}
				}
				break;
			case ' ':
			case '?':
			case '=':
			case ';':
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				escape += {c};
				break;
			default:
#ifdef DEBUG_ESCAPES
				std::cerr << "Unknown ANSI sequence: \"" << escape << (char)c << "\"\n";
#endif
				state = Normal;
				break; // not implemented
			}
			break;
		case OSC:
			escape += {c}; // todo utf8?
			if (escape.size() < 2 || escape.compare(escape.size() - 2, 2, "\033\\"))
				break;
			escape = escape.substr(0, escape.size() - 2);
			state = Normal;
			if (false) {
			}
#ifdef DEBUG_ESCAPES
			else {
				std::cerr << "Unknown OSC sequence: " << escape << "\n";
			}
#endif
		}
	}

	struct StringOptions {
		bool modeset = true;
		bool trim_trailers = false;
		bool visible_only = false;
		enum Buffer {
			ActiveBuffer, MainBuffer, AltBuffer
		} buffer = ActiveBuffer;

		StringOptions() {}
	};

	std::string to_string(StringOptions options = {}) {
		// note: trim_trailers currently does not account for cell backgrounds
		std::string result;
		Cell::Mode m = Cell::Mode();

		if (options.buffer == StringOptions::ActiveBuffer)
			options.buffer = alt_mode ? StringOptions::AltBuffer : StringOptions::MainBuffer;

		auto &buf = options.buffer == StringOptions::AltBuffer ? alt_buffer : buffer;

		std::vector<std::vector<Cell>> lines;
		if (!options.visible_only && options.buffer == StringOptions::MainBuffer)
			lines = scrollback;

		size_t re = height;
		for (;re > 0 && buf[re-1].empty(); re--);
		for (size_t ri = 0; ri < re; ri++)
			lines.push_back(buf[ri]);

		for (auto &r : lines) {
			size_t last = r.size();

			if (options.trim_trailers) {
				while (last && r[last - 1].character == " ")
					last--;
			}

			for (size_t i = 0; i < last; i++) {
				auto &cell = r[i];
				if (options.modeset)
					result += cell.mode.to_string(m);
				m = cell.mode;
				result += cell.character;
			}
			if (options.modeset) {
				if (Cell::Mode().to_string(m).size())
					result += "\033[m";
				m = Cell::Mode();
			}
			result+="\n";
		}
		result += Cell::Mode().to_string(m);
		return result;
	}
};

int main(int argc, char**argv) {
	Buffer::StringOptions options;

	std::vector<std::string> positionals = {"", ""};
	size_t positional_i = 0;

	for (int i = 1; i < argc; i++) {
		std::string arg = argv[i];
		if (arg == "--plain") {
			options.modeset = false;
			options.trim_trailers = true;
		} else if (arg == "--alt") {
			options.buffer = Buffer::StringOptions::AltBuffer;
		} else if (!arg.compare(0, 2, "--")) {
			std::cerr << "Unrecognised argument " << arg << "\n";
			return EXIT_FAILURE;
		} else {
			if (positional_i >= positionals.size()) {
				std::cerr << "Excess positional argument " << arg << "\n";
				return EXIT_FAILURE;
			}
			positionals[positional_i++] = arg;
		}
	}

	std::string filename = positionals[0];
	std::string timing = positionals[1];

	std::ifstream file;
	std::istream &input = filename == "" ? std::cin : (file.open(filename), file);

	// Timing file
	std::ifstream timing_file;
	bool use_timing = false;
	if (timing != "") {
		timing_file.open(timing);
		use_timing = true;
	}
	
	std::string line;
	std::getline(input, line);

	// Parse the script header for terminal dimensions
	// rfind is used in case the command passed to script matches
	size_t strpos;
#define EXTRACT(leader, format, result) \
	if ((strpos = line.rfind(leader "=\"")) != std::string::npos) \
		sscanf(line.data() + strpos + sizeof(leader) - 1 + 2, format "\"", result)
	
	size_t cols = 0, rows = 0;
	EXTRACT("COLUMNS", "%zu", &cols);
	EXTRACT("LINES", "%zu", &rows);

	bool feed_header = false;
	if (!cols || !rows) {
		cols = 88;
		rows = 24;
		feed_header = true;
	}

	Buffer buf = {cols, rows};
	if (feed_header) {
		for (char c : line)
			buf.input(c);
		buf.input('\n');
	}

	char c;
	if (use_timing) {
		double timestep;
		double chars;
		while (input.good()) {
			chars = ~0;
			timestep = 0;
			timing_file >> timestep >> chars;
			// todo make timing actually accurate
			std::this_thread::sleep_for(std::chrono::milliseconds((size_t)(timestep * 1000)));
			while (chars-- && input.good()) {
				input.get(c);
				buf.input(c);
			}
			std::string out = "\033[H\033[2J" + buf.to_string(options);
			out.pop_back(); // remove trailing newline

			auto pos = buf.get_pos();
			out += (std::stringstream{} <<
					"\033[" << (pos.row + 1) << ";" << (pos.col + 1) << "H").str();

			std::cout << out << std::flush;
		}
		std::cout << "\n";
	} else {
		while (input.get(c), input.good()) {
			buf.input(c);
		}
		std::cout << buf.to_string(options);
	}
}
