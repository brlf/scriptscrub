# Script Scrub

Cleans output from `script`.

## Building

Run `make`, or compile with the C++ compiler of your choice.

## Usage

```
<typescript scriptscrub
```

Emitting plain text:

```
<typescript scriptscrub --plain
```

scriptscrub can be set as your `less` preprocessor. Currently for non-script files it is hardcoded to wrap at 88 characters.

```
export LESSOPEN="|scriptscrub %s" LESS=R

less typescript

neofetch | less /dev/stdin
```

If you pass two arguments, the second one is taken to be the timing file, and it will play back the terminal session (similar to `asciinema`)
```
script -T typescript.timing -qc 'for i in {0..9}; do echo $i; sleep .$i; done'
scriptscrub typescript{,.timing}
```

## Why

Script records the exact output of commands. This means that there can be a lot of noise in the file from overwritten characters. It also means that programs that lay out their output using ANSI positioning sequences will not be laid out correctly just by stripping escape sequences.

scriptscrub simulates how writes would interact with a terminal buffer, then converts that to a string. This allows it to properly handle backtracking and overwriting.

## Current Limitations

### Wrong offsets
Some ANSI codes are dependent on the scroll position. For examples, jumps to absolute positions on the screen may end up in the wrong position (this is a limitation of blindly catting script as well). To fix this, make sure `script` starts with a cleared screen:

```
clear; script -qc foo
```

### Unicode
UTF-8 is sort of handled. Character width is not accounted for, so double-width characters will break output.

### Missing/differing behaviour
Only a handful of escape codes are currently implemented. Any escapes that aren't handled are simply dropped.

### Alternate buffer
The alternate buffer is not yet fully implemented.

## References
* [ANSI Escape Sequences](https://gist.github.com/fnky/458719343aabd01cfb17a3a4f7296797)
* [UTF-8](https://en.m.wikipedia.org/wiki/UTF-8)
* [Control Characters and Escape Sequences](https://terminalguide.namepad.de/seq/)
* [Hyperlinks in terminal emulators](https://gist.github.com/egmontkob/eb114294efbcd5adb1944c9f3cb5feda)
